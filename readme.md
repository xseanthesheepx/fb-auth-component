# FireBase Auth Web Components

StencilJS Web Components for FireBase Authentication

### Params

 color: 'primary' & 'secondary' - color of button
 label: name of button

 ex '<google-auth label="google log in" color="primary"></google-auth>'

<email-auth></email-auth>
<google-auth></google-auth>
<sign-up></sign-up>
