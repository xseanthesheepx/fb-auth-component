import { Component, Prop} from '@stencil/core';
import * as firebase from 'firebase';

@Component({
  tag: 'google-auth',
  styleUrl: 'google-auth.scss'
})
export class GoogleAuth {

@Prop() label: string;
@Prop() color: string;

bg:any;

  render() {
    if(this.color === "primary"){
      this.bg = {background: "#488aff"}
    }
    if(this.color === "secondary"){
      this.bg = {background: "#32db64"}
    }

    return (
      <button id="authButton" onClick={()=>{this.googleLogin()}} style={this.bg} >{this.label}</button>
    );
  }


  googleLogin(){
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
    console.log('logged in with google');
  }
}
