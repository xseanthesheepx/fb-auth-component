import { Component, Prop } from '@stencil/core';
import * as firebase from 'firebase';

@Component({
  tag: 'email-auth',
  styleUrl: 'email-auth.scss'
})

export class EmailAuth {


  @Prop() color: string;
  @Prop() label: string;

  email:string;
  password:string;
  bg:any;


  render() {

    if(this.color === "primary"){
      this.bg = {background: "#488aff"}
    }
    if(this.color === "secondary"){
      this.bg = {background: "#32db64"}
    }

    return (
      <div class="loginform">
           <div>
             <input id="email" type="text" placeholder="Email..."></input>
           </div>
           <div>
             <input id="password" type="password" placeholder="Password..."></input>
           </div>
           <div>
           <button id="signInBtn" onClick={()=>{this.logIn()}} style={this.bg}>{this.label}</button>
           </div>
      </div>
    );
  }
  logIn(){
    this.email = (document.getElementById('email')as HTMLInputElement).value;
    this.password = (document.getElementById('password')as HTMLInputElement).value;
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then(function(){
      console.log('logged in');
    }).catch(function(error) {
      var errorCode = error.code;
      console.log(errorCode)
    });
  }

}
