import { Component, Prop} from '@stencil/core';
import * as firebase from 'firebase';

@Component({
  tag: 'firebase-observer'
})
export class FirebaseObserver {

@Prop() label: string;
@Prop() color: string;

bg:any;

  render() {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;

        console.log('name: ', displayName,' email: ',email,' email verified: ',emailVerified,' photoURL: ',photoURL,' is anonymous: ',isAnonymous,' uid: ',uid,' provider data: ',providerData)

      } else {
        console.log('user is not logged in')
      }
    });
  }

}
