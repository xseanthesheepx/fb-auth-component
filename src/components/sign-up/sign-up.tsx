import { Component, Prop} from '@stencil/core';
import * as firebase from 'firebase';

@Component({
  tag: 'sign-up',
  styleUrl: 'sign-up.scss'
})
export class SignUp {

@Prop() label: string;
@Prop() color: string;

bg:any;

  render() {
    if(this.color === "primary"){
      this.bg = {background: "#488aff"}
    }
    if(this.color === "secondary"){
      this.bg = {background: "#32db64"}
    }

    return (
      <div class="signupform">
           <div>
             <input id="email"  placeholder="Email..."></input>
           </div>
           <div>
             <input id="password" type="password" placeholder="Password..."></input>
           </div>
           <div>
           <button id="signUpBtn" onClick={()=>{this.signUp()}} style={this.bg}>{this.label}</button>
           </div>
      </div>
    );
  }


  signUp(){
    let email = (document.getElementById('email')as HTMLInputElement).value;
    let password = (document.getElementById('password')as HTMLInputElement).value;
    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
      var errorCode = error.code;
      console.log(errorCode);
    });
    console.log(email)
  }

}
