
import { Component, Prop} from '@stencil/core';
import * as firebase from 'firebase';

@Component({
  tag: 'sign-out',
  styleUrl: 'sign-out.scss'
})
export class SignOut {

@Prop() label: string;
@Prop() color: string;

bg:any;

  render() {
    if(this.color === "primary"){
      this.bg = {background: "#488aff"}
    }
    if(this.color === "secondary"){
      this.bg = {background: "#32db64"}
    }

    return (
      <button id="authButton" onClick={()=>{this.logOut()}} style={this.bg} >{this.label}</button>
    );
  }

  logOut(){
    firebase.auth().signOut().then(function() {
    console.log('logged out')
    localStorage.clear();
  }).catch(function(error) {
    console.log(error)
  });
  }
}
