exports.config = {
  bundles: [
    { components: ['email-auth','google-auth','sign-up','sign-out','firebase-observer'] }
  ],
  collections: [
    { name: '@stencil/router' }
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**',
  httpPort: '3333'
}
